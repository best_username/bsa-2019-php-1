<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        foreach ($arena->fighters as $value) {
            $text .= '<h2>' . $value->getName() . ': ' . $value->getAttack() . ', ' . $value->getHealth() . '</h2><img src="' . $value->getImage() . '">';
        }
        return $text;
    }
}
