<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    public $fighters = [];
    
    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        usort($this->fighters, function($a, $b) {
            return $a->getAttack() < $b->getAttack() ? 1 : -1;
        });
        return $this->fighters[0];
    }

    public function mostHealthy(): Fighter
    {
        usort($this->fighters, function($a, $b) {
            return $a->getHealth() < $b->getHealth() ? 1 : -1;
        });
        return $this->fighters[0];
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
