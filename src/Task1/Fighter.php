<?php

declare(strict_types=1);

namespace App\Task1;

class Fighter
{
    private $id, $name, $health, $attack, $image;

    function __construct($id, $name, $health, $attack, $image) {
        $this->id = $id;
        $this->name = $name;
        $this->health = $health;
        $this->attack = $attack;
        $this->image = $image;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getAttack(): int
    {
        return $this->attack;
    }

    public function getImage(): string
    {
        return $this->image;
    }
    
    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setHealth($health) {
        $this->health = $health;
    }

    public function setAttack($attack) {
        $this->attack = $attack;
    }

    public function setImage($image) {
        $this->image = $image;
    }

}
